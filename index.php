<?php
// Parent class
// property yang ada di kelas Hewan adalah nama, darah dengan nilai default 50, jumlahKaki, dan keahlian.
// Method kelas Hewan : atraksi(), di dalam method ini akan menampilkan string nama dan keahlian. 
// Contoh "harimau_1 sedang lari cepat" atau "elang_3 sedang terbang tinggi".
abstract class Hewan {
  public $nama;
  public $darah = 50;
  public $jumlahKaki;
  public $keahlian;
  public function __construct($nama, $darah, $jumlahKaki, $keahlian) {
    $this->nama = $nama;
    $this->darah = $darah;
    $this->jumlahKaki = $jumlahKaki;
    $this->keahlian = $keahlian;
  }
  abstract public function atraksi($nama,$keahlian);
}

// Parent class
// property yang ada di kelas Fight adalah attackPower, defencePower
// Method kelas Fight
// // serang() , di dalam method ini akan menampilkan string sebagai contoh berikut. Contoh : "harimau_1 sedang menyerang elang_3" atau "elang_3 sedang menyerang harimau_2".
// // diserang(), di dalam method ini akan menampilkan string sebagai contoh berikut. Contoh : "harimau_1 sedang diserang" atau "elang_3 sedang diserang", kemudian hewan yang diserang akan berkurang darahnya dengan rumus :
// // //"darah sekarang - attackPower penyerang / defencePower yang diserang"
// // Catatan : method diserang() otomatis dipanggil jika method serang() dipanggil.
abstract class Fight {
    public $attackPower;
    public $defencePower;
    public function __construct($attackPower, $defencePower) {
      $this->attackPower = $attackPower;
      $this->defencePower = $defencePower;
    }
    abstract public function serang();
    abstract public function diserang();
  }
  

// Child classes
// Method kelas Elang dan Harimau :
//       getInfoHewan(), didalam method ini semua property yang ada di dalam kelas Hewan dan Fight ditampilkan , dan jenis hewan (Elang atau Harimau).
class Harimau extends Hewan {
  public function getInfoHewan(){ 
    echo $this->$jumlahkaki=4; 
    echo $this->$keahlian=" lari cepat"; 
  }   
  public function atraksi($nama,$keahlian) : string {
    return "$this->nama! sedang lari cepat";
  }
}

class Harimau_1 extends Fight {
  public function getInfoHewan(){ 
    echo $this->$attackPower=7; 
    echo $this->$defencePower=5; 
  }     
  public function serang() : string {
    return "$this->nama! sedang menyerang";
  }
  public function diserang() : string {
    return "$this->nama! sedang diserang";
  }    
}


class Elang extends Hewan {
  public function getInfoHewan(){ 
    echo $this->$jumlahkaki=2; 
    echo $this->$keahlian=" terbang tinggi"; 
  } 
  public function atraksi($nama,$keahlian) : string {
    return "$this->nama! sedang terbang tinggi";
  }  
}

class Elang_3 extends Fight {
  public function getInfoHewan(){ 
    echo $this->$attackPower=10; 
    echo $this->$defencePower=5; 
  }   
  public function serang($nama,$keahlian) : string {
    return "$this->nama! sedang menyerang";
  }
  public function diserang($nama,$keahlian) : string {
    return "$this->nama! sedang diserang";
  }     
}


// Ketika Harimau diintansiasi, maka jumlahKaki bernilai 4, dan keahlian bernilai "lari cepat" , attackPower = 7 , deffencePower = 8 ;
$Harimau_1 = new Harimau("Harimau");
echo $Harimau->atraksi();
echo "<br>";

// Ketika Elang diinstansiasi, maka jumlahKaki bernilai 2, dan keahlian bernilai "terbang tinggi", attackPower = 10 , deffencePower = 5 ;
$Elang_3 = new Elang("Elang");
echo $Elang->atraksi();
?>